import { render, screen } from '@testing-library/react';
import App from './App';

test('renders header', () => {
  render(<App />);
  const occupation = screen.getByText(/software developer/i);
  expect(occupation).toBeInTheDocument();
});
