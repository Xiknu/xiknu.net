import './App.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab, faMastodon, faGitAlt } from '@fortawesome/free-brands-svg-icons'
import { faComment } from '@fortawesome/free-solid-svg-icons'

library.add(fab)

function App() {
  return (
    <div className="App">
            <h1>
                Welcome to my website!
                </h1>
                <p>Welcome to my website! I am a software developer, system administrator and a security researcher! 
                </p>
        <hr></hr>
        <h1>Projects </h1>
            <p>The only active project I maintain at the moment is my website. I kind of hit programmer's block.</p>
        <hr></hr>
            <footer> 
            <a href={process.env.REACT_APP_MATRIX_URL}><FontAwesomeIcon icon={faComment} /></a> <a href={process.env.REACT_APP_MASTODON_URL} rel="me"><FontAwesomeIcon icon={faMastodon} /></a> <a href={process.env.REACT_APP_GIT_URL}><FontAwesomeIcon icon={faGitAlt} /></a>
            </footer>
            </div>
);
}


export default App;
