FROM node:alpine
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json package.json
COPY . ./
RUN npm i
RUN npm i -g serve
RUN npm run build
CMD ["serve", "-s", "build"]
